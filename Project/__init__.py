from flask import Flask, render_template
from flask_import Manager

app = Flask(__name__)
manager = Manager(app)

@app.route('/')
def hello_world():

    return '<h2 style="text-align: center; color:red;">Hello World!!</h2>'